# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Josip Ponjavic <josipponjavic at gmail dot com>
# Contributor: Xu Fasheng <fasheng.xu[AT]gmail.com>

pkgname=deepin-daemon
pkgver=6.1.14
pkgrel=1
pkgdesc='Daemon handling the DDE session settings'
arch=('x86_64')
url="https://github.com/linuxdeepin/dde-daemon"
license=('GPL3')
# deepin-session-shell not added for org.deepin.dde.LockFront1, because bring in
#                      deepin-session for non-DDE users is not desirable
depends=('deepin-desktop-schemas' 'deepin-api' 'deepin-app-services' 'deepin-wloutput-daemon'
         # TODO
         'gtk3' 'libx11' 'gdk-pixbuf-xlib' 'gdk-pixbuf2' 'libxi' 'libpulse' 'glib2' 'libxcursor'
         'libxcrypt' 'libgudev' 'pam' 'iso-codes'
         # accounts
         'accountsservice' 
         # accounts/grub
         'deepin-polkit-agent' 'deepin-polkit-agent-ext-gnomekeyring'
         # accounts/keybinding/systeminfo
         'deepin-desktop-base'
         # accounts1/user
         'deepin-desktop-theme'
         # appearance
         'fontconfig' 'noto-fonts'
         # audio/session
         'pulse-native-provider' 'alsa-lib'
         # bin/backlight_helper
         'ddcutil'
         # clipboard
         'libxfixes'
         # dock
         'bamf'
         # gesture
         'deepin-widgets'
         # image_effect
         'deepin-session-ui' 'sudo'
         # inputdevices
         'imwheel' 'libxkbfile' 'procps-ng' 'psmisc'
         # iw
         'libnl'
         # keybinding
         'gvfs'
         # keybinding/default-terminal, keybinding/shortcuts/system_shortcut.go
         'deepin-application-manager'
         # misc
         'librsvg'
         # network
         'mobile-broadband-provider-info' 'org.freedesktop.secrets' 'xdg-utils'
         # network/system
         'systemd' 'systemd-libs'
         # system
         'dmidecode' 'libinput' 'lshw' 'upower'
         # systeminfo
         'lsb-release' 'udisks2' 'util-linux')
makedepends=('sqlite' 'deepin-gettext-tools' 'git' 'mercurial' 'python-gobject' 'networkmanager'
             'bluez' 'go')
optdepends=('networkmanager: for network management support'
            'bluez: for bluetooth support'
            'iw: for miracast module'
            'proxychains-ng: for proxy configuration module')
conflicts=('dde-daemon')
replaces=('dde-daemon')
groups=('deepin')
install="$pkgname.install"
source=("git+https://github.com/linuxdeepin/dde-daemon.git#tag=$pkgver"
        $pkgname-fix-vanilla-libinput.patch)
sha512sums=('04972811983a61455bea26fb2e172a2c69cd584b3206e1299e425b660ef8593daead5f13d5609754b8068b4193a79bf04b6ba28b88e23869ca814614acaa7407'
            'b6006983bdb226726a4c4f44584ac4d58038532ea867a72646895978c37cf5fa7ed3f4981b5b936610750c021fb1a25ab3cf201095f9ebc0714964d1514f042a')

prepare() {
  cd dde-daemon
  patch -p1 -i ../$pkgname-fix-vanilla-libinput.patch

  # https://github.com/linuxdeepin/developer-center/discussions/3327
  sed -i 's#/usr/libexec#/usr/lib#' keybinding1/shortcuts/system_shortcut.go
  sed -i 's#${PREFIX}/libexec/#${PREFIX}/lib/#;s#${DESTDIR}/lib#${DESTDIR}${PREFIX}/lib#' Makefile

  sed -i 's|/etc/os-version|/etc/uos-version|' keybinding1/shortcuts/shortcut_manager.go
}

build() {
  cd dde-daemon

  export CGO_CPPFLAGS="${CPPFLAGS}"
  export CGO_CFLAGS="${CFLAGS}"
  export CGO_CXXFLAGS="${CXXFLAGS}"
  export CGO_LDFLAGS="${LDFLAGS}"
  export GOFLAGS="-buildmode=pie -trimpath -mod=readonly -modcacherw"
  
  # make -C network/nm_generator gen-nm-code
  make
}

package() {
  cd dde-daemon
  make DESTDIR="$pkgdir" PAM_MODULE_DIR=usr/lib/security install

  install -Dm644 debian/dde-daemon.sysusers "$pkgdir/usr/lib/sysusers.d/deepin-daemon.conf"
}
